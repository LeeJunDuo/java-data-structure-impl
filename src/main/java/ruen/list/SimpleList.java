package ruen.list;

public class SimpleList<E> {
    public class Node {
        Node(E elem) {
            this.elem = elem;
        }
        E elem;
        Node next;
    }

    private Node first;

    public void add(E elem) {
        Node n = new Node(elem);
        if (this.first == null) {
            this.first = n;
        } else {
            append(n);
        }
    }

    public void append(Node n) {
        Node last = first;
        while (last.next != null) {
            last = last.next;
        }
        last.next = n;
    }

    public Integer size() {
        Node last = first;
        Integer num = 0;

        while (last != null) {
            num++;
            last = last.next;
        }
        return num;
    }

    public E get(int i) {
        checkSize(i);
        return findElemByIndex(i);
    }

    private E findElemByIndex(int index) {
        Node last = findNodeByIndex(index);
        return last.elem;
    }

    public Node getNode(int i) {
        checkSize(i);
        return findNodeByIndex(i);
    }

    private Node findNodeByIndex(int index) {
        Node last = first;
        int i = 0;
        while (i != index) {
            last = last.next;
            i++;
        }
        return last;
    }

    private void checkSize(int i) throws IndexOutOfBoundsException {
        int size = size();
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException(String.format("Index: %d, Size: %d", i, size));
        }
    }

    @Override
    public String toString() {

        Node n = first;
        StringBuilder sb = new StringBuilder();

        do {
            sb.append(n.elem);
            sb.append(" ");
            n = n.next;
        } while (n != null);

        return sb.toString();
    }

    public static void main(String[] args) {
        SimpleList<Integer> list = new SimpleList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);

        Integer i = 0;
        while (i < list.size()) {
            System.out.println(list.get(i));
            i++;
        }
    }

    public void reverseList() {
       reverse(first);
    }

    private Node reverse(Node node) {
        Node next = node.next;
        if (next.next == null) {
            node = switchNode(node, next);
            first = next;
        } else {
            node = switchNode(node, reverse(next));
        }
        return node;
    }

    private Node switchNode(Node n1, Node n2) {
        n1.next = null;
        n2.next = n1;
        return n1;
    }


    public SimpleList.Node getNodeFromBottom(Integer seqNum) {
        int N = size();
        int index = N - seqNum;
        return getNode(index);
    }

    //
//	public SimpleList<E> deleteDuplicateNode(SimpleList<E> list) {
//
//	}

    public boolean isCircleList() {
        int N = size();
        int i = 1;
        Node n = first;
        boolean isCircle = false;

        outter:
        while (i < N && n.next != null) {
            Node cursor = n.next;

            do {
                if (n == cursor) {
                    isCircle = true;
                    break outter;
                }
                cursor = cursor.next;
            } while (cursor != null);
            n = n.next;

        }

        return isCircle;
    }


    public Node getCircularNode() {
        if (isCircleList()) {
            return null;
        } else {
            return null;
        }
    }
}
