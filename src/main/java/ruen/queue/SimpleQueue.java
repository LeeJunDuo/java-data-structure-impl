package main.java.ruen.queue;

public class SimpleQueue<E> {
	public class Node {
		public Node(E elem) {
			this.elem = elem;
		}

		E elem;
		Node previous;
		Node next;
	}

	Node first;
	Node last;

	public void enqueue(E elem) {
		Node n = new Node(elem);
		if (this.isEmpty()) {
			first = n;
			last = n;
			first.next = last;
			last.previous = first;
		} else {
			Node t = last;
			t.next = n;
			n.previous = t;
			last = n;
		}
	}

	public void dequeue() {
		if (first.next.equals(last)) {
			first = null;
			last = null;
		} else {
			Node n = first.next;
			first = n;
		}
	}

	public boolean isEmpty() {
		return (first == null && last == null);
	}

	public E top() throws NullPointerException {
		return first.elem;
	}

	private boolean isJustOne() {
		return first == last;
	}

	@Override
	public String toString() {
		Node n = first;
		StringBuilder sb = new StringBuilder();

		if (n != null) {
			do {
				sb.append(n.elem);
				sb.append(" ");
				n = n.next;
			} while (n != last);
		} else if (isJustOne()) {
			sb.append(n.elem);
		} else {
			sb.append("No element in this queue");
		}
		return sb.toString();
	}
}
