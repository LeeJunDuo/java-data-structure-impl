package ruen.list;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SimpleListTest {
	SimpleList list = new SimpleList();

	@Test
	public void testSize() {
		assertEquals(0, list.size());
	}

	@Test
	public void testReverse() {

		list.add(0);
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.reverseList();
	}
}

