package ruen.list;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FinListTest {

	FinList list;
	int TANK_SIZE = 10;

	@BeforeAll
	void init() {
		this.list = new FinList(TANK_SIZE);
	}

	@Test
	void testNodeSize() {
		assertEquals(1, this.list.getNodeSize());
	}

	@Test
	void testInsertElementInNode() {
		list.insert(0);
		list.insert(0);
		assertEquals(2, list.first.tankIndex);
	}

	@Test
	void testInsertToFull() {
		insertUntilFull();
		assertEquals(TANK_SIZE, list.first.tankIndex);
	}

	@Test
	void testNewNodeWithInsert() {
		insertUntilFull();
		insertUntilFull();
		insertUntilFull();
		list.insert(0);
		assertEquals(4, this.list.getNodeSize());
		assertEquals(1, list.currentNode.tankIndex);
	}

	private void insertUntilFull(){
		int t = TANK_SIZE;
		while (t != 0) {
			list.insert(0);
			t--;
		}
	}
}
